from app import app,db
from flask import render_template,request
from app.models import User


@app.route('/')
def index():
    print("main page")
    return render_template("index.html",title="WAPP")

@app.route('/somepage')
def somepage():
    return render_template("somepage.html",title="WAPP")

@app.route('/login',methods=['POST'])
def login():
    response = request.form.to_dict()
    return None